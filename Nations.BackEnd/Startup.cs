﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nations.BackEnd.Startup))]
namespace Nations.BackEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
