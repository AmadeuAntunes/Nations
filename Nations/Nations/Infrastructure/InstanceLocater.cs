﻿namespace Nations.Infrastructure
{
    using ViewModels;
    public class InstanceLocater 
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        #region Constructores

        public InstanceLocater()
        {
            this.Main = new MainViewModel();
        }
        #endregion
    }
}
