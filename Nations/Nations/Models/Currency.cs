﻿namespace Nations.Models
{
    using Newtonsoft.Json;
    public class Currency
    {
        [JsonProperty(propertyName:"code")]
        public string Code { get; set; }

        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }

        [JsonProperty(propertyName: "symbol")]
        public string Symbol { get; set; }
    }
}
