﻿using Newtonsoft.Json;

namespace Nations.Models
{
    public class Translations
    {
        [JsonProperty(propertyName: "de")]
        public string German { get; set; }

        [JsonProperty(propertyName: "es")]
        public string Spanish { get; set; }

        [JsonProperty(propertyName: "fr")]
        public string French { get; set; }

        [JsonProperty(propertyName: "ja")]
        public string Japanese { get; set; }

        [JsonProperty(propertyName: "it")]
        public string Itallian { get; set; }

        [JsonProperty(propertyName: "br")]
        public string Brazilian { get; set; }

        [JsonProperty(propertyName: "pt")]
        public string Portuguese { get; set; }

        [JsonProperty(propertyName: "nl")]
        public string Dutch { get; set; }

        [JsonProperty(propertyName: "hr")]
        public string Croation { get; set; }

        [JsonProperty(propertyName: "fa")]
        public string Arabic { get; set; }
    }
}
