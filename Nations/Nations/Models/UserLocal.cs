﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nations.Models
{
    public class UserLocal
    {
        [PrimaryKey]
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public override int GetHashCode()
        {
            return UserId;
        }
        
    }
}
