﻿namespace Nations.Services
{
    using Nations.Models;
    using System.Threading.Tasks;
    using System;
    using System.Net.Http;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Plugin.Connectivity;
    using System.Text;

    public class ApiService
    {

        public async Task<Response> checkConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Please see your Internet Config"
                };
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Please see your Internet Connection."
                };
            }

            return new Response
            {
                IsSucess = true,
                Message = "Ok"
            };
        }


        public async Task<TokenResponse> GetToken(
           string urlBase, 
           string userName,
           string password)
        {
            try
            {
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri(urlBase);
                var response = await cliente.PostAsync("Token",
                    new StringContent(
                        string.Format("grant_type=password&userName={0}&password={1}", userName, password),
                        Encoding.UTF8,
                        "application/x-www-form-urlencoded"
                        )
                 );

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
       

        public async Task<Response>GetList<T>(
            string urlBase, 
            string servicePrefix,
            string controller)
        {
            try
            {
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await cliente.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSucess = false,
                        Message = result
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);
                return new Response()
                {
                    IsSucess = true,
                    Message = "OK",
                    Result = list
                };

            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }
         

        }
    }
}
