﻿
namespace Nations.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using Nations.Models;
    using Nations.Services;
    using Xamarin.Forms;
    using System.Collections.Generic;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using System.Linq;
    using Views;

    public class CountriesViewModel : BaseViewModel
    {

        #region Services

        private ApiService apiService;

        #endregion

        #region Atributos
        private ObservableCollection<CountryItemViewModel> _countries;    //Lista de Telemovel
        private bool _isRefreshing;
       // private List<Country> _countriesList;
        private string _filter;
       
        #endregion

        #region Propriedades


        public  string Filter
        {
            get { return _filter; }
            set {
                this.Search();
                SetValue(ref this._filter, value); }
        }


        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set {
                    SetValue(ref this._isRefreshing, value);
                }
        }

        public ObservableCollection<CountryItemViewModel> Countries
        {
            get { return _countries; }
            set {
                SetValue(ref this._countries, value);
            }
        }



        #endregion

        #region Commands

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }

        private async void Search()
        {

            if(string.IsNullOrEmpty(this.Filter))
            {
                this.Countries = new ObservableCollection<CountryItemViewModel>(this.TocountriesItemViewList());
            }
            else
            {
                this.Countries = new ObservableCollection<CountryItemViewModel>(
                    this.TocountriesItemViewList().Where(c=> c.Name.ToLower().Contains(this._filter.ToLower()) || 
                                                  c.Capital.ToLower().Contains(this._filter.ToLower() )
                                              )
                    );
            }

           
        }

        private IEnumerable<CountryItemViewModel> TocountriesItemViewList()
        {
            return MainViewModel.GetInstance().CountriesList.Select(c => new CountryItemViewModel
            {
                Alpha2Code = c.Alpha2Code,
                Alpha3Code = c.Alpha3Code,
                AltSpellings = c.AltSpellings,
                Area = c.Area,
                Borders = c.Borders,
                CallingCodes = c.CallingCodes,
                Capital = c.Capital,
                Cioc = c.Cioc,
                Currencies = c.Currencies,
                Demonym = c.Demonym,
                Flag = c.Flag,
                Gini = c.Gini,
                Languages = c.Languages,
                Latlng = c.Latlng,
                Name = c.Name,
                NativeName = c.NativeName,
                NumericCode = c.NumericCode,
                Population = c.Population,
                Region = c.Region,
                RegionalBlocs = c.RegionalBlocs,
                Subregion = c.Subregion,
                Timezones = c.Timezones,
                TopLevelDomain = c.TopLevelDomain,
                Translations = c.Translations

            });
        }

        public ICommand RefreshCommand
        {
            get
            {
                
                return new RelayCommand(LoadCountries);
            }
        }

        #endregion

        #region Construtor
        public CountriesViewModel()
        {
          
            this.apiService = new ApiService();
            this.LoadCountries();
           
        }
        #endregion

        private async void LoadCountries()
        {
            IsRefreshing = true;
            /*Testar a connecção*/

            var connection = await this.apiService.checkConnection();
            if(!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    connection.Message,
                    "OK");

                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            var response = await this.apiService.GetList<Country>(
                "http://restcountries.eu",
                "/rest",
                "/v2/all"
                );

            if (!response.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", response.Message, "OK");
                return;

            }

            MainViewModel.GetInstance().CountriesList = (List<Country>)response.Result;
            this.Countries = new ObservableCollection<CountryItemViewModel>(TocountriesItemViewList());
            IsRefreshing = false;
        }
    }
}
