﻿

namespace Nations.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Nations.Models;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Views;

    public class CountryItemViewModel : Country
    {
        public ICommand SelectCountryCommand
        {
            get { return new RelayCommand(SelectCountry); }
        }

   
        private async void SelectCountry()
        {
            MainViewModel.GetInstance().Country = new CountryViewModel(this);
           
           await Application.Current.MainPage.Navigation.PushAsync(new CountryTabbedPage());
        }
    }
}
