﻿
namespace Nations.ViewModels
{
    using Nations.Models;
    using System.Collections.ObjectModel;
    using System.Linq;

    public class CountryViewModel:BaseViewModel
    {
        #region Atributes
        private ObservableCollection<Border> borders;

        private ObservableCollection<Currency> currency;

        private ObservableCollection<Language> languages;
        #endregion


        #region properties


        public ObservableCollection<Language> Languages
        {
            get { return languages; }
            set
            {
                SetValue(ref this.languages, value);
            }
        }



        public ObservableCollection<Currency> Currency
        {
            get { return currency; }
            set
            {
                SetValue(ref this.currency, value);
            }
        }



        public ObservableCollection<Border> Borders
        {
            get { return borders; }
            set {
                SetValue(ref this.borders, value);
            }
        }
        public Country Country { get; set; }

        #endregion



        public CountryViewModel(CountryItemViewModel country)
        {
            this.Country = country;
            Currency = new ObservableCollection<Currency>(this.Country.Currencies);
            Languages = new ObservableCollection<Language>(this.Country.Languages);
            LoadBoards();
        }

        private  void LoadBoards()
        {
            borders = new ObservableCollection<Border>();

            foreach (var border in Country.Borders)
            {
                var country = MainViewModel.GetInstance()
                    .CountriesList
                    .FirstOrDefault(c => c.Alpha3Code == border);
                    
                if(this.Borders != null)
                {
                    this.Borders.Add(new Border
                    {
                        Code = country.Alpha3Code,
                        Name = country.Name
                    });
                }

            }

        }
    }

   
}
