﻿namespace Nations.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Nations.Services;
    using Nations.Views;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel:BaseViewModel
    {

        #region Events

      // public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Atributes

        private bool _isRunning;
        private bool _isRemebered;
        private bool _isEnable;
        private string _email;
        private string _password;
        private ApiService ApiService;

        #endregion

        #region Properties

        public bool IsRunning
        {
            get { return _isRunning; }

            set
            {
                SetValue(ref this._isRunning, value);
            }
        }

        public bool IsRemebered
        {
            get { return _isRemebered; }

            set
            {
                SetValue(ref this._isRemebered, value);
            }
        }

        public bool IsEnable
        {
            get { return _isEnable; }

            set
            {
                SetValue(ref this._isEnable, value);
            }
        }

        public string Email
        {
            get { return _email; }

            set
            {
                SetValue(ref this._email, value);
            }
        }

        public string Password
        {
            get { return _password; }

            set
            {
                SetValue(ref this._password, value);
            }
        }
        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        public ICommand LoginFacebookCommand
        {
            get
            {
                return new RelayCommand(LoginFacebook);
            }
        }

        private async void LoginFacebook()
        {
            await Application.Current.MainPage.DisplayAlert("Facebook", "Not Done, yet", "OK");
            return;
        }


        private async void Login()
        {
            if(string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You must enter email, please", "OK");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You must enter password, please", "OK");
                return;
            }

            this.IsRunning = true;
            this.IsEnable = false;


            var connection = await this.ApiService.checkConnection();
            if(!connection.IsSucess)
            {
                this._isRunning = false;
                this.IsEnable = true;
                await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "OK");
                return;
            }


            var token = await this.ApiService.GetToken("http://nationsapiaa.azurewebsites.net/", this.Email, this.Password);
            if(token == null)
            {
                this._isRunning = false;
                this.IsEnable = true;
                await Application.Current.MainPage.DisplayAlert("Error", "Something was wrong, please try later.", "OK");
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this._isRunning = false;
                this.IsEnable = true;
                await Application.Current.MainPage.DisplayAlert("Error",token.ErrorDescription , "OK");
                return;
            }

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;

            this.Password = string.Empty;
            this.IsRunning = false;
            this.IsEnable = true;
            this.Email = string.Empty;

            mainViewModel.Countries = new CountriesViewModel();

            await Application.Current.MainPage.Navigation.PushAsync(new CountriesPage());

        }

        #endregion

        




        #region Constructores

        public LoginViewModel()
        {
            this.ApiService = new ApiService();
            this.IsRemebered = true;
            this.IsEnable = true;
          
        }

        #endregion


    }
}
