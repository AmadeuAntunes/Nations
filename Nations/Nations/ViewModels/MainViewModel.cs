﻿namespace Nations.ViewModels
{
    using Nations.Models;
    using System.Collections.Generic;
    public class MainViewModel
    {
        #region ViewModels

        public LoginViewModel Login { get; set; }
            
        public CountriesViewModel Countries { set; get; }

        public CountryViewModel Country { set; get; }

       


        #endregion

        #region Properties
        public List<Country> CountriesList { get; set; }
        public TokenResponse Token { get; set; }
        #endregion

        #region Constructores

        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
        }

        #endregion

        #region Singleton

        private static MainViewModel instance;

  
     

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            else
            {
                return instance;
            }

        }

        #endregion
    }
}
